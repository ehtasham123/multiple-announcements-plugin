import java.util.StringTokenizer;

import com.audium.server.sayitsmart.SayItSmartBase;
import com.audium.server.sayitsmart.SayItSmartContent;
import com.audium.server.sayitsmart.SayItSmartDependency;
import com.audium.server.sayitsmart.SayItSmartDisplay;
import com.audium.server.sayitsmart.SayItSmartException;
import com.audium.server.sayitsmart.SayItSmartPlugin;
/**
 * 
 * @author Malik Ehtasham
 * <br>
 * <h3> Discription </h3>
 * This is a custom plugin for SayitSmart.<br>
 * This plugin is basically used to play list of audio prompts. <br>
 * An array String will be passed to this method <br>
 * This String array can be passed through session variable<br>
 * This session variable contain list of prompts.<br>
 * This plugin use that array and play the prompts.
 *
 */

public class Arrayprompt extends SayItSmartBase implements SayItSmartPlugin {
	String description = "This is simple Testing";
    /**
     * This method is used to define how this plugin will appear in Builder for 
     * Studio.
     */
	public SayItSmartDisplay getDisplayInformation() throws SayItSmartException
	{
		// PUT YOUR CODE HERE.
		
		SayItSmartDisplay toReturn = new SayItSmartDisplay("String_array", "List_of_Prompts", description);
        toReturn.addInputFormat("String_array", "List_of_Prompts", "This is a testing Input String");
        toReturn.addOutputFormat("Prompts_of_array", "List_of_Prompts", "This is a testing Output String");
        toReturn.addFileset("CheckFileSet", "Stander_File", "This is a Simple File Set");
		return toReturn;
    }
    
    /**
     * The input and output format dependencies are defined herasdf. 
     */
    public SayItSmartDependency getFormatDependencies() throws SayItSmartException
    {
		// PUT YOUR CODE HERE.
        SayItSmartDependency toReturn = new SayItSmartDependency("String_array", new String[] {
                "Prompts_of_array"
        });
        return toReturn;
    }
    
    /**
     * The output format and fileset dependencies are defined here. 
     */
    public SayItSmartDependency getFilesetDependencies() throws SayItSmartException
    {
		// PUT YOUR CODE HERE.
        SayItSmartDependency toReturn = new SayItSmartDependency("Prompts_of_array", new String[] {
                "CheckFileSet"
        });
        return toReturn;
    }
    
    /**
     * This method actually does the conversion, returning a SayItSmartContent 
     * object listing all the audio files and/or TTS phrases needed to speak the
     * value passed as input.
     */
    public SayItSmartContent convertToFiles(Object dataAsObject, String inputFormat, 
    		String outputFormat, String fileset) throws SayItSmartException
    {    
		// PUT YOUR CODE HERE.
    		SayItSmartContent toReturn = new SayItSmartContent();
    		Object[] arr=(Object[])dataAsObject;
    		for(int i=0;i<arr.length;i++)
    		{
    			System.out.println(arr[i]+"Object data");
    			toReturn.add(arr[i]+"",arr[i]+"",false); 
    		}

    		return toReturn;
    }
    
    /**
     * This method returns what files need to be recorded for each filset. At 
     * this stage, this method is not used by Builder for Studio, but should be 
     * provided for future compatibility. Return null if the content is so 
     * dynamic that the number of audio files cannot be predicted.
     */
    public String[] getFilesetContents(String fileset) throws SayItSmartException
    {
		// PUT YOUR CODE HERE.
        return   new String[] {
                "hundred","1st"
        };
    }
}

    
    
    
    
